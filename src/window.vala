[GtkTemplate (ui="/co/edu/uniquindio/hello-world/ui/window.ui")]
public class Window : Gtk.ApplicationWindow
{
	private HelloWorld app;
	
	[GtkChild] private unowned Gtk.Label label;
	
	public Window(HelloWorld app)
	{
		this.app = app;
	}
	
	[GtkCallback] public void print_hello()
	{
		label.set_text("You have clicked owo");
	}
}
