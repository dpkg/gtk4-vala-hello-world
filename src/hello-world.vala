using Gtk;
public class HelloWorld : Gtk.Application 
{
	private Window window;
	
	private const ActionEntry[] app_entries =
	{
		{ "about", about_cb, null, null, null },
	};
	
	public HelloWorld()
	{
	    Object(
		application_id: "co.edu.uniquindio.HelloWorldGTK4",
		flags : ApplicationFlags.FLAGS_NONE
	    );
	}
	
	private void create_window()
	{
		window = new Window(this);
		add_window(window);
	}
	
	
	private void about_cb()
	{
		string [] authors =
		{
			"Mateo Estrada Ramirez",
			null
		};
		
		string [] documenters =
		{
			"Mateo Estrada Ramirez",
			null
		};
		
		show_about_dialog (window,
                               "program-name", _("Hello World"),
                               "title", _("About Hello World"),
                               "version", VERSION,
                               "comments",
                               /* Short description in the about dialog */
                               _("Little hello world using Vala and GTK4"),
                               "logo-icon-name", "computer",
                               "authors", authors,
                               "documenters", documenters);
	}
	
	
	protected override void startup()
	{
		base.startup();
		
		create_window();
		
		add_action_entries (app_entries, this);
	}

	protected override void activate () 
	{	
		base.activate();
		window.present ();
	}
	
	public override void shutdown ()
	{
		base.shutdown ();
	}

}
